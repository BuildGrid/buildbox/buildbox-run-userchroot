FROM registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildbox:nightly

COPY . /buildbox-run-userchroot

RUN cd /buildbox-run-userchroot && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=DEBUG .. && \
    make && \
    CTEST_OUTPUT_ON_FAILURE=1 make test && \
    make install

ENV PATH "/buildbox-run-userchroot/build:$PATH"

ENV CASD_BINARY=buildbox-casd
ENV RUNNER_BINARY=buildbox-run-userchroot
ENV WORKER_BINARY=buildbox-worker

ENV INSTANCE_NAME=""

# Set urls
ENV BUILDGRID_SERVER_URL="http://controller:50051"
ENV CAS_SERVER_URL=${BUILDGRID_SERVER_URL}
ENV LOGSTREAM_SERVER_URL=""
ENV LOGSTREAM_SERVER_INSTANCE_NAME=""
ENV BIND_LOCATION=127.0.0.1:50011

# Set stager
ENV BUILDBOX_STAGER=fuse

# Set platform properties
ARG DEFAULT_PLATFORM_OPTS="--platform OSFamily=linux --platform ISA=x86-64"
ENV PLATFORM_PROPERTY_OPTS=${DEFAULT_PLATFORM_OPTS}

# Touch a known path to inject into the chroot to test path injection
RUN mkdir -p -m 0755 /testing/mapped-directory && \
    touch /testing/known-path && \
    touch /testing/mapped-directory/known-path && \
    mkdir -p -m 0755 /testing/mapped-directory/subdirectory && \
    touch /testing/mapped-directory/subdirectory/a-file && \
    chown -R runuser:wheel /testing

# Default entry point
CMD /entry-points/worker-start.sh
