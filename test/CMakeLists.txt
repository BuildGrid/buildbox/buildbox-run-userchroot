find_file(BuildboxGTestSetup.cmake BuildboxGTestSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxGTestSetup.cmake})

set(target userchrootrunner_tests)

add_executable(${target}
  userchroot.t.cpp
  chrootmanager.t.cpp
  commandfilemanager.t.cpp
  ../buildboxrun-userchroot/buildboxrun_userchroot.cpp
  ../buildboxrun-userchroot/buildboxrun_chrootmanager.cpp
  ../buildboxrun-userchroot/buildboxrun_commandfilemanager.cpp
)

target_include_directories(${target} PRIVATE ../buildboxrun-userchroot)
target_link_libraries(${target} Buildbox::buildboxcommon ${GTEST_MAIN_TARGET} ${GTEST_TARGET} ${GMOCK_TARGET})
add_test(NAME ${target}
         COMMAND ${target}
         WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/data)
